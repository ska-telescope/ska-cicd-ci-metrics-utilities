-include .make/base.mk
-include .make/python.mk

PYTHON_LINT_TARGET = src/ tests/
PYTHON_SWITCHES_FOR_PYLINT = --max-args 10
