import os
import shutil
from datetime import datetime

import anybadge

from ska_ci_metrics_utilities.create_badges import create_and_write_badge


def main():
    # Create badges directory
    badges_dir = "docs/src/badges"
    if os.path.exists(badges_dir):
        shutil.rmtree(badges_dir)
    os.makedirs(badges_dir)

    # Define thresholds dictionaries
    test_error_thresholds = {1: "green", 6: "yellow", 999999: "red"}
    test_failure_thresholds = {1: "green", 999999: "red"}
    test_skipped_thresholds = {1: "green", 6: "yellow", 999999: "red"}
    coverage_thresholds = {50: "red", 60: "orange", 80: "yellow", 100: "green"}
    lint_error_thresholds = {1: "green", 6: "yellow", 999999: "red"}
    lint_failure_thresholds = {1: "green", 999999: "red"}

    # Generate and write build status badges
    create_and_write_badge(
        "last build",
        "unknown",
        filename=os.path.join(badges_dir, "build_last_date_unknown.svg"),
    )
    create_and_write_badge(
        "last build",
        "2023/07/27 12:34:56",
        thresholds={},
        filename=os.path.join(badges_dir, "build_last_date_known.svg"),
    )

    # Generate and write tests badges
    create_and_write_badge(
        "tests error",
        "unknown",
        filename=os.path.join(badges_dir, "tests_error_unknown.svg"),
        thresholds=test_error_thresholds,
    )
    create_and_write_badge(
        "tests error",
        "0",
        filename=os.path.join(badges_dir, "tests_error_success.svg"),
        thresholds=test_error_thresholds,
    )
    create_and_write_badge(
        "tests error",
        "5",
        filename=os.path.join(badges_dir, "tests_error_warning.svg"),
        thresholds=test_error_thresholds,
    )
    create_and_write_badge(
        "tests error",
        "6",
        filename=os.path.join(badges_dir, "tests_error_fail.svg"),
        thresholds=test_error_thresholds,
    )

    create_and_write_badge(
        "tests failures",
        "unknown",
        filename=os.path.join(badges_dir, "tests_failures_unknown.svg"),
        thresholds=test_failure_thresholds,
    )
    create_and_write_badge(
        "tests failures",
        "0",
        filename=os.path.join(badges_dir, "tests_failures_success.svg"),
        thresholds=test_failure_thresholds,
    )
    create_and_write_badge(
        "tests failures",
        "1",
        filename=os.path.join(badges_dir, "tests_failures_fail.svg"),
        thresholds=test_failure_thresholds,
    )

    create_and_write_badge(
        "tests skipped",
        "unknown",
        filename=os.path.join(badges_dir, "tests_skipped_unknown.svg"),
        thresholds=test_skipped_thresholds,
    )
    create_and_write_badge(
        "tests skipped",
        "0",
        filename=os.path.join(badges_dir, "tests_skipped_success.svg"),
        thresholds=test_skipped_thresholds,
    )
    create_and_write_badge(
        "tests skipped",
        "5",
        filename=os.path.join(badges_dir, "tests_skipped_warning.svg"),
        thresholds=test_skipped_thresholds,
    )
    create_and_write_badge(
        "tests skipped",
        "6",
        filename=os.path.join(badges_dir, "tests_skipped_fail.svg"),
        thresholds=test_skipped_thresholds,
    )

    # Generate and write test totals badge
    create_and_write_badge(
        "tests total",
        "unknown",
        filename=os.path.join(badges_dir, "tests_total_unknown.svg"),
    )
    create_and_write_badge(
        "tests total",
        "1",
        thresholds={},
        filename=os.path.join(badges_dir, "tests_total_known.svg"),
    )

    # Generate and write coverage badges
    create_and_write_badge(
        "coverage",
        "unknown",
        filename=os.path.join(badges_dir, "coverage_unknown.svg"),
        thresholds=coverage_thresholds,
    )
    create_and_write_badge(
        "coverage",
        100,
        filename=os.path.join(badges_dir, "coverage_success.svg"),
        thresholds=coverage_thresholds,
    )
    create_and_write_badge(
        "coverage",
        80,
        filename=os.path.join(badges_dir, "coverage_warning1.svg"),
        thresholds=coverage_thresholds,
    )
    create_and_write_badge(
        "coverage",
        60,
        filename=os.path.join(badges_dir, "coverage_warning2.svg"),
        thresholds=coverage_thresholds,
    )
    create_and_write_badge(
        "coverage",
        50,
        filename=os.path.join(badges_dir, "coverage_warning3.svg"),
        thresholds=coverage_thresholds,
    )
    create_and_write_badge(
        "coverage",
        0,
        filename=os.path.join(badges_dir, "coverage_fail.svg"),
        thresholds=coverage_thresholds,
    )

    # Generate and write lint badges
    create_and_write_badge(
        "lint errors",
        "unknown",
        filename=os.path.join(badges_dir, "lint_errors_unknown.svg"),
        thresholds=lint_error_thresholds,
    )
    create_and_write_badge(
        "lint errors",
        "0",
        filename=os.path.join(badges_dir, "lint_errors_success.svg"),
        thresholds=lint_error_thresholds,
    )
    create_and_write_badge(
        "lint errors",
        "5",
        filename=os.path.join(badges_dir, "lint_errors_warning.svg"),
        thresholds=lint_error_thresholds,
    )
    create_and_write_badge(
        "lint errors",
        "6",
        filename=os.path.join(badges_dir, "lint_errors_fail.svg"),
        thresholds=lint_error_thresholds,
    )

    create_and_write_badge(
        "lint failures",
        "unknown",
        filename=os.path.join(badges_dir, "lint_failures_unknown.svg"),
        thresholds=lint_failure_thresholds,
    )
    create_and_write_badge(
        "lint failures",
        "0",
        filename=os.path.join(badges_dir, "lint_failures_success.svg"),
        thresholds=lint_failure_thresholds,
    )
    create_and_write_badge(
        "lint failures",
        "1",
        filename=os.path.join(badges_dir, "lint_failures_fail.svg"),
        thresholds=lint_failure_thresholds,
    )

    # Generate and write lint total badge
    create_and_write_badge(
        "lint total",
        "unknown",
        filename=os.path.join(badges_dir, "lint_total_unknown.svg"),
    )
    create_and_write_badge(
        "lint total",
        "1",
        thresholds={},
        filename=os.path.join(badges_dir, "lint_total_known.svg"),
    )


if __name__ == "__main__":
    main()
