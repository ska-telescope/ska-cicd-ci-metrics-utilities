#!/bin/bash

if [ -z "$CI_METRICS_REF" ]; then
    CI_METRICS_REF=master
fi

echo "Using CI_METRICS_REF=${CI_METRICS_REF}"

mkdir -p build/badges
pip3 install anybadge

echo "Collecting Metrics..."
curl -s --retry 30 --retry-delay 3 --header "PRIVATE-TOKEN: $CI_JOB_TOKEN" \
 https://gitlab.com/ska-telescope/ska-cicd-ci-metrics-utilities/raw/${CI_METRICS_REF}/src/ska_ci_metrics_utilities/collect_metrics.py | python3 -u
echo "Done"

echo "Creating Badges..."
curl -s --retry 30 --retry-delay 3 --header "PRIVATE-TOKEN: $CI_JOB_TOKEN" \
 https://gitlab.com/ska-telescope/ska-cicd-ci-metrics-utilities/raw/${CI_METRICS_REF}/src/ska_ci_metrics_utilities/create_badges.py | python3 -u
echo "Done"
