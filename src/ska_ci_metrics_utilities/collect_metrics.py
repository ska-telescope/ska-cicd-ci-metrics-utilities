"""Script to collect and parse CI metrics."""
import json
import logging
import os
import sys
import xml.etree.ElementTree as etree
from datetime import datetime, timezone

LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level="INFO", format=LOGGING_FORMAT)
logger = logging.getLogger("ci-metrics")


def to_int(value: str, default: int = 0) -> int:
    "Convert value to int"
    try:
        return int(value)
    except ValueError:
        logger.error("Failed parsing coverage property:", exc_info=True)
        return default


def to_float(value: str, default: float = 0) -> float:
    "Convert value to float"
    try:
        return float(value)
    except ValueError:
        logger.error("Failed parsing coverage property:", exc_info=True)
        return default


def parse_junit_tree(element, test_summary=None):
    """Recursively parse and extract a summary of a JUnit xml element tree.

    Args:
        element (xml.etree.Element): XML Element
        test_summary (dict): Summary of the JUnit XML report

    Returns:
        dict: Summary of Junit results.

    Notes:
        For JUnit XML schema see:
            <https://github.com/windyroad/JUnit-Schema>
    """
    # Create default entries in the stats dictionary if required.
    if not test_summary:
        test_summary = {
            "testsuites": {"tests": 0, "failures": 0},
            "testsuite": {
                "tests": 0,
                "failures": 0,
                "skipped": 0,
                "errors": 0,
            },
            "testcase": {"tests": 0, "failures": 0, "skipped": 0, "errors": 0},
        }

    # Parse top level <testsuites> or <testsuite> tags.
    if element.tag in ["testsuites", "testsuite"]:
        for attr in test_summary[element.tag]:
            test_summary[element.tag][attr] += int(element.get(attr, 0))

        for child in element:
            parse_junit_tree(child, test_summary)

    # Parse <testcase> tag. This is a child of testsuite.
    if element.tag == "testcase":
        key = "testcase"

        # Incrememnt test case counter.
        test_summary[key]["tests"] += 1

        # Parse child <error>, <skipped>, <failure> tags.
        for child in element:
            parse_junit_tree(child, test_summary)

    # Parse <error>, <skipped>, and <failure> tags. Children of testcase.
    if element.tag in ["error", "skipped", "failure"]:
        key = element.tag
        if element.tag == "error":
            key = "errors"
        elif element.tag == "failure":
            key = "failures"
        test_summary["testcase"][key] += 1

    # Parse <system-out>, <system-err>, and <properties> tags.
    # Children of testsuite.
    if element.tag in ["system-out", "system-err", "properties"]:
        pass

    return test_summary


def count_junit_metrics(filename):
    """Collect metrics from a JUnit XML file.

    Used to parse unit tests and linting results.

    Args:
        filename (str): Filename path of JUnit file

    Returns:
        dict: Summary of Unit test or linting results

    """
    try:
        root_elem = etree.parse(filename).getroot()
        if root_elem.tag not in ["testsuites", "testsuite"]:
            raise ValueError("Invalid JUnit XML file.")

        stats = parse_junit_tree(root_elem)
        result = {"errors": 0, "failures": 0, "tests": 0, "skipped": 0}
        for key in result:
            if key in ["tests", "failures"]:
                result[key] = max(
                    stats["testsuites"][key],
                    stats["testsuite"][key],
                    stats["testcase"][key],
                )
            else:
                result[key] = max(
                    stats["testsuite"][key], stats["testcase"][key]
                )
        result["total"] = result["tests"]
        del result["tests"]
    except FileNotFoundError:
        logger.warning("%s metrics file not found", filename)

        return {
            "errors": "unknown",
            "failures": "unknown",
            "total": "unknown",
            "skipped": 0,
        }

    except (etree.ParseError, ValueError) as expt:
        logger.error(
            "Failed parsing '%s': %s",
            filename,
            expt,
        )

        return {
            "errors": "unknown",
            "failures": "unknown",
            "total": "unknown",
            "skipped": 0,
        }

    return result


def parse_iso8601_rfc3339(datetime_str: str) -> datetime:
    "Parse iso8601 with RFC3339 datetimes"
    try:
        return datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%f%z")
    except ValueError:
        return datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S%z")


def get_latest_build_timestamp():
    "Gets the latest build timestamp from the pipeline creation date"
    latest_build_timestamp = datetime.now(timezone.utc).timestamp()
    try:
        env_pipeline_created = os.environ.get("CI_PIPELINE_CREATED_AT", None)
        if env_pipeline_created:
            latest_build_timestamp = parse_iso8601_rfc3339(
                env_pipeline_created
            ).timestamp()
    except ValueError:
        logger.error("Failed getting latest build timestamp:", exc_info=True)

    return latest_build_timestamp


def parse_coverage(filename):
    """Parse the coverage report to return the percentage coverage.

    Returns:
        int or str: coverage percentage or 'unknown'
    """
    cov_tree = None
    cov_percent = None

    try:
        cov_tree = etree.parse(filename)
    except FileNotFoundError:
        logger.warning("%s coverage file not found", filename)

    if cov_tree:
        try:
            cov_root = cov_tree.getroot()
            branch_count = cov_root.get("branches-valid", "0")
            if branch_count != "0":
                branch_count = to_int(branch_count)
                branches_covered = to_int(
                    cov_root.get("branches-covered", "0")
                )
                line_count = to_int(cov_root.get("lines-valid", "0"))
                lines_covered = to_int(cov_root.get("lines-covered", "0"))

                # This is how coverage.py's
                # terminal report calculates TOTAL coverage:
                numerator = lines_covered + branches_covered
                denominator = line_count + branch_count
                cov_percent = 100.0 * numerator / denominator
            else:
                cov_percent = 100.0 * to_float(cov_root.get("line-rate", "0"))

        except AttributeError as err:
            logger.warning(
                "Attribute not found. Make sure that the file "
                "code-coverage.xml has the correct 'line-rate' "
                "attribute: %s",
                err,
            )

    return cov_percent


def main():
    """Build ci-metric.json file from JUnit reports."""
    # pylint: disable=broad-except, too-many-locals
    # Exit if 'ci-metrics.json' already exists
    ci_metrics_fp = "build/reports/ci-metrics.json"
    if os.path.isfile(ci_metrics_fp):
        logger.info(
            "ci-metrics.json file already exists, using the data "
            "present in that file."
        )
        sys.exit(0)
    else:
        os.makedirs(os.path.dirname(ci_metrics_fp), exist_ok=True)

    # Read CI environment variables
    env_commit_sha = os.environ["CI_COMMIT_SHA"]
    latest_build_timestamp = get_latest_build_timestamp()

    # Parse coverage report
    coverage = parse_coverage("build/reports/code-coverage.xml")

    # Parse e2e coverage report
    e2e_coverage = parse_coverage("build/reports/e2e-coverage.xml")

    # Parse JUnit unit XML tests report
    test_result = count_junit_metrics("build/reports/unit-tests.xml")

    # Parse JUnit unit XML e2e tests report
    e2e_test_result = count_junit_metrics("build/reports/e2e-tests.xml")

    # Parse JUnit XML linting report
    lint_result = count_junit_metrics("build/reports/linting.xml")

    # Create and write data object with all the collected info
    ci_metrics_data = {
        "commit-sha": env_commit_sha,
        "build-status": {"last": {"timestamp": latest_build_timestamp}},
        "coverage": {"percentage": coverage},
        "e2e_coverage": {"percentage": e2e_coverage},
        "tests": test_result,
        "e2e_tests": e2e_test_result,
        "lint": lint_result,
    }

    with open(ci_metrics_fp, "w", encoding="utf-8") as write_file:
        json.dump(ci_metrics_data, write_file)


if __name__ == "__main__":
    main()
