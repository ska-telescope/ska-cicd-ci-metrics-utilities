"""Unit tests for metric collection.

Run with:
    ```
    pip install tox
    tox
    ```
or
    ```
    export PYTHONPATH=scripts
    pytest [-vv] [-s]
    ```
"""
import os
import xml.etree.ElementTree as etree
from datetime import datetime, timezone

import pytest

from ska_ci_metrics_utilities.collect_metrics import (
    count_junit_metrics,
    get_latest_build_timestamp,
    parse_coverage,
    parse_junit_tree,
)


def test_count_junit_metrics_success():
    """Test successful collection of linter metrics."""
    filename = "./tests/data/example-linting.xml"
    lint_result = count_junit_metrics(filename)
    assert lint_result == {
        "errors": 0,
        "failures": 52,
        "skipped": 0,
        "total": 15,
    }

    filename = "./tests/data/example-linting.xml"
    unit_test_result = count_junit_metrics(filename)
    assert unit_test_result == {
        "errors": 0,
        "failures": 52,
        "skipped": 0,
        "total": 15,
    }


def test_parse_tree_success():
    """Test sucessful parsing of JUnit XML tree."""
    filename = "./tests/data/example-linting.xml"
    stats = parse_junit_tree(etree.parse(filename).getroot())
    assert stats["testsuites"] == {"tests": 0, "failures": 0}
    assert stats["testsuite"] == {
        "tests": 15,
        "failures": 52,
        "skipped": 0,
        "errors": 0,
    }
    assert stats["testcase"] == {
        "tests": 15,
        "failures": 52,
        "skipped": 0,
        "errors": 0,
    }

    filename = "./tests/data/example-unit-tests.xml"
    stats = parse_junit_tree(etree.parse(filename).getroot())
    assert stats["testsuites"] == {"tests": 0, "failures": 0}
    assert stats["testsuite"] == {
        "tests": 52,
        "failures": 0,
        "skipped": 0,
        "errors": 0,
    }
    assert stats["testcase"] == {
        "tests": 52,
        "failures": 0,
        "skipped": 0,
        "errors": 0,
    }


def test_count_junit_metrics_invalid_data():
    """Test output of Junit metrics function with invalid data file."""
    result = count_junit_metrics("./tests/data/invalid.xml")
    assert result == {
        "errors": "unknown",
        "failures": "unknown",
        "skipped": 0,
        "total": "unknown",
    }


def test_parse_coverage():
    """Test output of"""
    result = parse_coverage("./tests/data/code-coverage-with-wrong-fields.xml")
    assert result == 0

    result = parse_coverage("./tests/data/code-coverage-without-branch.xml")
    assert result == 88.17

    result = parse_coverage("./tests/data/code-coverage-with-branch.xml")
    assert result == pytest.approx(84.6806)

    result = parse_coverage("./tests/data/code-coverage-nonexistent.xml")
    assert not result


def test_get_latest_build_timestamp():
    """Test latest build timestamp"""
    now = datetime.now(timezone.utc).timestamp()

    result = get_latest_build_timestamp()
    assert result

    os.environ["CI_PIPELINE_CREATED_AT"] = "WRONG-FORMAT"
    result = get_latest_build_timestamp()
    assert result >= now

    os.environ["CI_PIPELINE_CREATED_AT"] = "2022-01-31T16:47:55Z"
    result = get_latest_build_timestamp()
    assert (
        result
        == datetime(
            year=2022,
            month=1,
            day=31,
            hour=16,
            minute=47,
            second=55,
            microsecond=0,
        ).timestamp()
    )

    os.environ["CI_PIPELINE_CREATED_AT"] = "2022-01-31T16:47:55.345Z"
    result = get_latest_build_timestamp()
    assert (
        result
        == datetime(
            year=2022,
            month=1,
            day=31,
            hour=16,
            minute=47,
            second=55,
            microsecond=345000,
        ).timestamp()
    )

    del os.environ["CI_PIPELINE_CREATED_AT"]
    result = get_latest_build_timestamp()
    assert result >= now
