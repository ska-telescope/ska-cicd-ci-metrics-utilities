"test_create_badges tests badge creation"

import os
import shutil
import xml.etree.ElementTree as ET

import pytest

from ska_ci_metrics_utilities import create_badges as cb

BUILD_PATH = "build/badges"

# Define global variables for colors
COLOR_UNKNOWN = "#FF00FF"
COLOR_GREEN = "#4C1"
COLOR_YELLOW = "#DFB317"
COLOR_ORANGE = "#FE7D37"
COLOR_RED = "#E05D44"
COLOR_NAVY = "#000080"


@pytest.fixture(scope="function")
def setup_fixture():
    """
    Setup test fixture
    """
    if os.path.exists(BUILD_PATH):
        shutil.rmtree(BUILD_PATH)

    os.makedirs(BUILD_PATH)
    yield
    shutil.rmtree(BUILD_PATH)


def _test_svg_color(svg_filename, expected_color):
    """
    Tests the color of a badge by looking into the SVG
    """
    tree = ET.parse(svg_filename)
    root = tree.getroot()
    search_key = f'{{http://www.w3.org/2000/svg}}g/\
{{http://www.w3.org/2000/svg}}\
path[@fill="{expected_color}"]'

    elements = root.findall(search_key)
    assert elements


def test_create_and_write_badge_unknown_value():
    """Test for creating and writing a badge with an unknown value"""
    badge_filename = os.path.join(BUILD_PATH, "test_badge_unknown.svg")
    value = cb.create_and_write_badge(
        "test label", "unknown", filename=badge_filename
    )
    assert os.path.exists(badge_filename)
    assert value == "unknown"

    # Test SVG color for unknown value
    _test_svg_color(badge_filename, "#FF00FF")


def test_create_and_write_badge_none_value():
    """Test for creating and writing a badge with an unknown value"""
    badge_filename = os.path.join(BUILD_PATH, "test_badge_unknown.svg")
    value = cb.create_and_write_badge(
        "test label", None, filename=badge_filename
    )
    assert os.path.exists(badge_filename)
    assert value == "unknown"

    # Test SVG color for unknown value
    _test_svg_color(badge_filename, "#FF00FF")


def test_create_and_write_badge_with_thresholds():
    """Test for creating and writing a badge with thresholds"""
    thresholds = {0: "green", 50: "yellow", 100: "red"}
    badge_filename = os.path.join(BUILD_PATH, "test_badge_threshold.svg")
    cb.create_and_write_badge(
        "test label", 25, thresholds=thresholds, filename=badge_filename
    )
    assert os.path.exists(badge_filename)

    # Test SVG color for value 25 with thresholds
    _test_svg_color(badge_filename, COLOR_YELLOW)


def test_generate_build_status_badges_unknown():
    """Test for generating build status badges with unknown timestamp"""
    test_ci_metrics = {"build-status": {"last": {"timestamp": None}}}
    value = cb.generate_build_status_badges(test_ci_metrics)

    unknown_svg_file = os.path.join(BUILD_PATH, "build_last_date.svg")
    assert os.path.exists(unknown_svg_file)
    assert value == "unknown"

    # Test SVG color for unknown timestamp
    _test_svg_color(unknown_svg_file, "#FF00FF")


def test_generate_build_status_badges_not_defined():
    """Test for generating build status badges with unknown timestamp"""
    test_ci_metrics = {}
    cb.generate_build_status_badges(test_ci_metrics)

    unknown_svg_file = os.path.join(BUILD_PATH, "build_last_date.svg")
    assert os.path.exists(unknown_svg_file)

    # Test SVG color for unknown timestamp
    _test_svg_color(unknown_svg_file, "#FF00FF")


def test_generate_build_status_badges_known():
    """Test for generating build status badges with known timestamp"""
    test_ci_metrics = {"build-status": {"last": {"timestamp": 1679870572}}}
    cb.generate_build_status_badges(test_ci_metrics)

    known_svg_file = os.path.join(BUILD_PATH, "build_last_date.svg")
    assert os.path.exists(known_svg_file)

    # Test SVG color for known timestamp
    _test_svg_color(known_svg_file, COLOR_NAVY)


def test_generate_tests_badges_unknown():
    """Test for generating test badges with unknown values"""
    test_ci_metrics = {
        "tests": {
            "errors": "unknown",
            "failures": "unknown",
            "skipped": "unknown",
            "total": "unknown",
        }
    }
    cb.generate_tests_badges(test_ci_metrics)

    # Test SVG color for unknown values
    _test_svg_color(os.path.join(BUILD_PATH, "tests_errors.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "tests_failures.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "tests_skipped.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "tests_total.svg"), "#FF00FF")


def test_generate_tests_badges_not_defined():
    """Test for generating test badges with unknown values"""
    test_ci_metrics = {}
    cb.generate_tests_badges(test_ci_metrics)

    # Test SVG color for unknown values
    _test_svg_color(os.path.join(BUILD_PATH, "tests_errors.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "tests_failures.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "tests_skipped.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "tests_total.svg"), "#FF00FF")


def test_generate_tests_badges_fail():
    """Test for generating test badges with failing values"""
    test_ci_metrics = {
        "tests": {
            "errors": 10,
            "failures": 10,
            "skipped": 10,
            "total": 0,
        }
    }
    cb.generate_tests_badges(test_ci_metrics)

    _test_svg_color(os.path.join(BUILD_PATH, "tests_errors.svg"), COLOR_RED)
    _test_svg_color(os.path.join(BUILD_PATH, "tests_failures.svg"), COLOR_RED)
    _test_svg_color(os.path.join(BUILD_PATH, "tests_skipped.svg"), COLOR_RED)
    _test_svg_color(os.path.join(BUILD_PATH, "tests_total.svg"), COLOR_RED)


def test_generate_tests_badges_success():
    """Test for generating test badges with success values"""
    test_ci_metrics = {
        "tests": {
            "errors": 0,
            "failures": 0,
            "skipped": 0,
            "total": 10,
        }
    }
    cb.generate_tests_badges(test_ci_metrics)

    _test_svg_color(os.path.join(BUILD_PATH, "tests_errors.svg"), COLOR_GREEN)
    _test_svg_color(
        os.path.join(BUILD_PATH, "tests_failures.svg"), COLOR_GREEN
    )
    _test_svg_color(os.path.join(BUILD_PATH, "tests_skipped.svg"), COLOR_GREEN)
    _test_svg_color(os.path.join(BUILD_PATH, "tests_total.svg"), COLOR_NAVY)


def test_generate_tests_badges_warning():
    """Test for generating test badges with warning values"""
    test_ci_metrics = {
        "tests": {
            "errors": 5,
            "failures": 0,
            "skipped": 5,
            "total": 10,
        }
    }
    cb.generate_tests_badges(test_ci_metrics)

    _test_svg_color(os.path.join(BUILD_PATH, "tests_errors.svg"), COLOR_YELLOW)
    _test_svg_color(
        os.path.join(BUILD_PATH, "tests_failures.svg"), COLOR_GREEN
    )
    _test_svg_color(
        os.path.join(BUILD_PATH, "tests_skipped.svg"), COLOR_YELLOW
    )
    _test_svg_color(os.path.join(BUILD_PATH, "tests_total.svg"), COLOR_NAVY)


def test_generate_coverage_badge_unknown():
    """Test for generating coverage badge with unknown value"""
    test_ci_metrics = {
        "coverage": {
            "percentage": "unknown",
        }
    }
    cb.generate_coverage_badge(test_ci_metrics)

    # Test SVG color for unknown value
    _test_svg_color(os.path.join(BUILD_PATH, "coverage.svg"), "#FF00FF")


def test_generate_coverage_badge_not_defined():
    """Test for generating coverage badge with unknown value"""
    test_ci_metrics = {"coverage": {}}
    cb.generate_coverage_badge(test_ci_metrics)

    # Test SVG color for unknown value
    _test_svg_color(os.path.join(BUILD_PATH, "coverage.svg"), "#FF00FF")


def test_generate_coverage_badge_green():
    """Test for generating coverage badge with high value"""
    test_ci_metrics = {
        "coverage": {
            "percentage": 80.765543,
        }
    }
    cb.generate_coverage_badge(test_ci_metrics)

    # Test SVG color for known value
    _test_svg_color(os.path.join(BUILD_PATH, "coverage.svg"), COLOR_GREEN)


def test_generate_coverage_badge_yellow():
    """Test for generating coverage badge with warning value"""
    test_ci_metrics = {
        "coverage": {
            "percentage": 60.12345,
        }
    }
    cb.generate_coverage_badge(test_ci_metrics)

    # Test SVG color for known value
    _test_svg_color(os.path.join(BUILD_PATH, "coverage.svg"), COLOR_YELLOW)


def test_generate_coverage_badge_orange():
    """Test for generating coverage badge with warning value"""
    test_ci_metrics = {
        "coverage": {
            "percentage": 50.0000,
        }
    }
    cb.generate_coverage_badge(test_ci_metrics)

    # Test SVG color for known value
    _test_svg_color(os.path.join(BUILD_PATH, "coverage.svg"), COLOR_ORANGE)


def test_generate_coverage_badge_red():
    """Test for generating coverage badge with failing value"""
    test_ci_metrics = {
        "coverage": {
            "percentage": 49.9999,
        }
    }
    cb.generate_coverage_badge(test_ci_metrics)

    # Test SVG color for known value
    _test_svg_color(os.path.join(BUILD_PATH, "coverage.svg"), COLOR_RED)


def test_generate_lint_badges_unknown():
    """Test for generating lint badges with unknown values"""
    test_ci_metrics = {
        "lint": {
            "errors": "unknown",
            "failures": "unknown",
            "total": "unknown",
        }
    }
    cb.generate_lint_badges(test_ci_metrics)

    # Test SVG color for unknown values
    _test_svg_color(os.path.join(BUILD_PATH, "lint_errors.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "lint_failures.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "lint_total.svg"), "#FF00FF")


def test_generate_lint_badges_not_defined():
    """Test for generating lint badges with unknown values"""
    test_ci_metrics = {}
    cb.generate_lint_badges(test_ci_metrics)

    # Test SVG color for unknown values
    _test_svg_color(os.path.join(BUILD_PATH, "lint_errors.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "lint_failures.svg"), "#FF00FF")
    _test_svg_color(os.path.join(BUILD_PATH, "lint_total.svg"), "#FF00FF")


def test_generate_lint_badges_success():
    """Test for generating lint badges with success values"""
    test_ci_metrics = {
        "lint": {
            "errors": 0,
            "failures": 0,
            "total": 2,
        }
    }
    cb.generate_lint_badges(test_ci_metrics)

    _test_svg_color(os.path.join(BUILD_PATH, "lint_errors.svg"), COLOR_GREEN)
    _test_svg_color(os.path.join(BUILD_PATH, "lint_failures.svg"), COLOR_GREEN)
    _test_svg_color(os.path.join(BUILD_PATH, "lint_total.svg"), COLOR_NAVY)


def test_generate_lint_badges_warning():
    """Test for generating lint badges with warning values"""
    test_ci_metrics = {
        "lint": {
            "errors": 5,
            "failures": 10,
            "total": 20,
        }
    }
    cb.generate_lint_badges(test_ci_metrics)

    _test_svg_color(os.path.join(BUILD_PATH, "lint_errors.svg"), COLOR_YELLOW)
    _test_svg_color(os.path.join(BUILD_PATH, "lint_failures.svg"), COLOR_RED)
    _test_svg_color(os.path.join(BUILD_PATH, "lint_total.svg"), COLOR_NAVY)
